Webmapper deployment
====================

Provision a Debian based system with a baseline configuration needed to run 
Webmapper.

The deployment depends on a few things. 


  * knowing the target user's password
  * having sudo installed on the remote host (adding the user to group sudo)

This last one is important because we'll have to set up software that requires
root access.

*Step 0*

install webmapper-fabric-deploy

    $ git clone ssh://git@bitbucket.org/svevang/webmapper-fabric-deploy.git
    $ cd webmapper-fabric-deploy
    $ virtualenv .env
    $ . .env/bin/activate
    $ pip install -r requirements.txt



*Step 1*

Run the fabric command to deploy the site

    $ fab deploy_webmapper:my_project_name -Huser@some_host

This command will install necessary servers and create a template django 
project in `~/django_projects/my_project_name`. In addition a virtualenv
will be created in `~/environments/MY_PROJECT_NAME/`. 

Thats it. Take a look at the fabfile for further details on how to integrate
with your deployment strategy.


Deployment with Vagrant
-----------------------

Useful if you want to deploy the webmapper stack and develop on vagrant:


# install vagrant

    local$ echo "source :rubygems \n gem 'vagrant'" > Gemfile
    local$ bundle

    local$ bundle exec vagrant box add debian_squeeze_32 http://mathie-vagrant-boxes.s3.amazonaws.com/debian_squeeze_32.box
    local$ bundle exec vagrant init debian_squeeze_32
    local$ bundle exec vagrant up
    local$ bundle exec vagrant ssh
    local$ bundle exec vagrant ssh_config >> ~/.ssh/config

# set a password for vagrant user

    vagrant$ sudo passwd

# enable password auth in /etc/ssh/sshd_config;

    vagrant$ sudo vi /etc/ssh/sshd_config
    vagrant$ sudo /etc/init.d/ssh restart
    vagrant$ ^d

# get port number from ssh_congfig output (was 2222 for me)

    local$ bundle exec vagrant ssh_config

# and deploy!

    local$ fab provision_webmapper_project:my_project -H vagrant@localhost:2222
