from distutils.core import setup

setup(
        name='webmapper-fabric-deploy',
        version='0.0.1',
        packages=['webmapper_fabric',],
        license='Public',
        long_description=open('README.md').read(),
        requires=['fabric', 'fabtools'],
        package_data={'webmapper_fabric': ['create_template_postgis-debian1.sh']}
      )
