webmapper_packages = ['libgeos-dev',
                      'libogdi3.2',
                      'libgdal-dev',
                      'libproj-dev',
                      'build-essential',
                      'libxml2-dev',
                     ]

postgresql_backports = ['postgresql-9.1',
                        'postgresql-server-dev-9.1',
                        'postgresql-common', ]
