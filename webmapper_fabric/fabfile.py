import os
from getpass import getpass

from fabric.api import env, run, settings, execute
from fabric.context_managers import hide, cd, prefix
from fabric.operations import sudo, put
from fabric.contrib.files import exists
from fabric.decorators import with_settings

from webmapper_env import webmapper_env

from system_dependencies import webmapper_packages
from system_dependencies import postgresql_backports

from fabtools import postgres

# populate our env with webmapper specific items
env.update(webmapper_env)

def setup_squeeze_backports():
    source_list_path = '/etc/apt/sources.list.d/backports.list'
    source_apt_path = "deb http://backports.debian.org/debian-backports squeeze-backports main"
    if not exists(source_list_path):
        sudo('touch {0}'.format(source_list_path))
        sudo('echo "{0}" > {1}'.format(source_apt_path, source_list_path))

def install_linux_packages():

    sudo('aptitude update')
    sudo('aptitude install -y {0}'.format(" ".join(webmapper_packages)))
    sudo('aptitude -y -t squeeze-backports install {0}'.format(" ".join(postgresql_backports)))

def create_postgis_template_db():
    script_name = 'create_template_postgis-debian1.sh'
    script_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), script_name)
    put(script_path,  remote_path="/tmp")
    postgres._run_as_pg('sh /tmp/'+script_name)

def install_postgis():
    sudo('aptitude install -y postgresql-9.1-postgis')


def provision_webmapper_host():
    setup_squeeze_backports()
    install_linux_packages()
    install_postgis()
    create_postgis_template_db()

def provision_webmapper_project(project_name=None):
    provision_webmapper_host()
    # create virtual_env
    # create django project 
    name = env.get(project_name, False) or project_name or "example_project"


